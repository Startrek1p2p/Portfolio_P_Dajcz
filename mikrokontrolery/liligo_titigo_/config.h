#define LILYGO_WATCH_2020_V1                 // To use T-Watch2020 V1, please uncomment this line
#include <LilyGoWatch.h>

#include <WiFi.h>

bool flag_wifi = 0;
WiFiServer wifiServer(100);

// Set your Static IP address
IPAddress local_IP(192, 168, 1, 105);
// Set your Gateway IP address
IPAddress gateway(192, 168, 1, 1);

IPAddress subnet(255, 255, 255, 0);

const uint16_t port = 100;
WiFiClient client;

const char* ssid = "";
const char* password =  "";

unsigned long previousMillis = 0;
unsigned long interval = 3000; // czas po którym jak wi-fi sie rozłaczy próbuje właczyc sie ponowanie
byte odliczanko = 0;

char receiveData() {
  Serial.print("Connected with: ");
  char c;
  Serial.print(client.remoteIP());
  Serial.print(", ");
  while (client.connected()) {
    while (client.available() > 0) {
      c = client.read();
    }
    delay(10);
  }
  Serial.println(c);
  client.stop();
  Serial.println("Petent disconnected");
  return c;
}


void initWiFi() {
  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  }
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
    odliczanko++;
    if (odliczanko > 10) {
      Serial.println("nadchodzi reset");
      ESP.restart();
    }
  }
  Serial.println(WiFi.localIP());
  wifiServer.begin();

}
