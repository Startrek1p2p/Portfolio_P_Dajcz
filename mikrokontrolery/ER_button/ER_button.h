#ifndef ER_BUTTON_H_INCLUDED
#define ER_BUTTON_H_INCLUDED

#include <Arduino.h>

class Button
{
    public:
        // Button(pin, Debounce_Time, PullUp, reverse).
        // Required:
        // pin      The Arduino pin the button is connected to
        // Optional:
        // Debounce_Time   Debounce time in milliseconds (50ms)
        // PullUp true to enable pullup resistor (true)
        // reverse  true to interpret a low as pressed (true)
        Button(uint8_t pin, uint32_t Debounce_Time=50, uint8_t PullUp=true, uint8_t reverse=true)
            : m_pin(pin), m_Debounce_Time(Debounce_Time), m_PullUp(PullUp), m_reverse(reverse) {}

        // Initialize a Button object
        void begin();

        // Returns the current debounced button state: true-pressed false-released
        bool read();

        // Returns true-pressed at the last call to read().
        //button must be read.
        bool isPressed();

        // Returns true at the last call to read() was pressed,
        // and this was a change since the previous read.
        bool wasPressed();

        // Returns true if the button state at the last call to read() was pressed,
        // given number of milliseconds.
        bool pressedFor(uint32_t ms);


        // Returns true-released at the last call to read().
        //button must be read.
        bool isReleased();

        // Returns true at the last call to read() was released,
        // and this was a change since the previous read.
        bool wasReleased();

        // Returns true if the button state at the last call to read() was released,
        // given number of milliseconds.
        bool releasedFor(uint32_t ms);


        // Returns the time in milliseconds (from millis) that the button last changed state.
        uint32_t lastChange();

    private:
        uint32_t m_Debounce_Time;       // debounce time (ms)
        uint32_t m_time;                // time of current state (ms - millis)
        uint32_t m_lastChange;          // time of last state change (ms)
        uint8_t m_pin;                  // button pin number
        bool m_PullUp;                  // pullup resistor enabled
        bool m_reverse;                 // if true - low as pressed, false high as pressed
        bool m_state;                   // current button state, true=pressed
        bool m_lastState;               // previous button state
        bool m_changed;                 // state changed since last read
};

//class ToggleButton "push-on, push-off" (toggle).
class ToggleButton : public Button
{
    public:
    
        // constructor like Button, but initialState for the toggle.
        ToggleButton(uint8_t pin, uint32_t Debounce_Time=50, uint8_t PullUp=true, uint8_t reverse=true, bool initialState=false)
            : Button(pin, Debounce_Time, PullUp, reverse), m_toggleState(initialState) {}

        // read the button and return its state.
        bool read()
        {
            Button::read();
            if (wasPressed())
            {
                m_toggleState = !m_toggleState;
                m_changed = true;
            }
            else
            {
                m_changed = false;
            }
            return m_toggleState;
        }

        // the state changed?
        bool changed() {return m_changed;}

        // the current state
        bool toggleState() {return m_toggleState;}

    private:
        bool m_toggleState;
        bool m_changed;
};
#endif