#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QtWidgets>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)

{
    ui->setupUi(this);
    is_avialable = false;
    port_name = "";
    arduino = new QSerialPort;


     //ui->textBrowser->setText("73289436");

     ui->elektronika_2->setStyleSheet(kolozebateszare);
     ui->elektronika_3->setStyleSheet(kolozebateszare);
     ui->elektronika->setStyleSheet(kolozebateszare);

     ui->bateria->setStyleSheet(bateriaszara);
     ui->bateria_2->setStyleSheet(bateriaszara);
     ui->bateria_3->setStyleSheet(bateriaszara);

     ui->bezpieczniki->setStyleSheet(bezpiecznikszary);
     ui->bezpieczniki_2->setStyleSheet(bezpiecznikszary);
     ui->bezpieczniki_3->setStyleSheet(bezpiecznikszary);

     ui->elektronika_4->setStyleSheet(kolozebateszare);
     ui->bezpieczniki_4->setStyleSheet(bezpiecznikszary);
     ui->bateria_4->setStyleSheet(bateriaszara);

     //ui->czarny_ekran->setVisible(false);
    ui->textBrowser_6->setVisible(false);

    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        if(serialPortInfo.hasVendorIdentifier() && serialPortInfo.hasProductIdentifier()){
// tutaj powinno byc sprawdzanie czy vendor i product id sie zgadzaja z naszym ukomputerem             if(serialPortInfo.vendorIdentifier())
        port_name =serialPortInfo.portName();
        is_avialable = true;
        }
    }

    if (is_avialable){
                //otworz i skonfigoroj port
        arduino->setPortName(port_name);
        arduino->open(QSerialPort::ReadWrite); /* WriteOnly jezeli tylo my mu piszemy, ReadOnly jezeli tylko on wysyła ReadWrite jedno i drugie*/
        arduino->setBaudRate(QSerialPort::Baud9600);
        arduino->setDataBits(QSerialPort::Data8);
        arduino->setParity(QSerialPort::NoParity);
        arduino->setStopBits(QSerialPort::OneStop);
        arduino->setFlowControl(QSerialPort::NoFlowControl);

        connect(this->arduino, SIGNAL(readyRead()),
                this, SLOT(readFromPort()));

    }else{
        //brak uradzenia
        QMessageBox::warning(this, "port error", "nie mozna znależć ukomputera");
    }
    this->centralWidget()->setStyleSheet(
       "background-image:url(://bg-kwadrat-rpi.png); background-position: center;background-repeat: no-repeat;" );
}

MainWindow::~MainWindow()
{
    if(arduino->isOpen()){
        arduino->close();
    }
    delete ui;
}

void MainWindow::Statek(QString WysPoz)
{
    if(WysPoz == "0 "){
        aktualneokienko=0;
        ui->pushButton->setStyleSheet("background: none;border-image: url(://gora-statek.png);");
        if(rozwiazane_1==0){
            ui->textBrowser->setStyleSheet(niebieskaramka);
            ui->elektronika_4->setStyleSheet("background: none;border-image: url(://kolozebate_nieb.png);");
        }if(rozwiazane_2==0){
        ui->textBrowser_3->setStyleSheet(bialaramka);
        ui->bateria_4->setStyleSheet(bateriaszara);
        }if(rozwiazane_3==0){
            ui->textBrowser_4->setStyleSheet(bialaramka);
            ui->bezpieczniki_4->setStyleSheet(bezpiecznikszary);
        }
    }else if(WysPoz =="1 "){
        aktualneokienko=1;
        ui->pushButton->setStyleSheet("background: none;border-image: url(://bok-statek.png);");
        if(rozwiazane_1==0){
            ui->textBrowser->setStyleSheet(bialaramka);
        ui->elektronika_4->setStyleSheet(kolozebateszare);
        }if(rozwiazane_2==0){
        ui->textBrowser_3->setStyleSheet(bialaramka);
        ui->bateria_4->setStyleSheet(bateriaszara);
        }if(rozwiazane_3==0){
            ui->textBrowser_4->setStyleSheet(niebieskaramka);
            ui->bezpieczniki_4->setStyleSheet("background: none;border-image: url(://bezpieczniik_nieb.png);");
        }
    }else if(WysPoz == "2 "){
        aktualneokienko=2;
        ui->pushButton->setStyleSheet("background: none;border-image: url(://tyl-statek.png);");
        if(rozwiazane_1==0){
            ui->textBrowser->setStyleSheet(bialaramka);
        ui->elektronika_4->setStyleSheet(kolozebateszare);
        }if(rozwiazane_2==0){
        ui->textBrowser_3->setStyleSheet(niebieskaramka);
        ui->bateria_4->setStyleSheet("background: none;border-image: url(://bateria_nieb.png);");
        }if(rozwiazane_3==0){
            ui->textBrowser_4->setStyleSheet(bialaramka);
            ui->bezpieczniki_4->setStyleSheet(bezpiecznikszary);
        }
    }
}

void MainWindow::Haslo(QString DoWyswietleniaHaslo)
{
    if(aktualneokienko ==1){
ui->pushButton->setStyleSheet("background: none;border-image: url(://bok-statek.png);");
if(rozwiazane_1==0){
    ui->textBrowser->setStyleSheet(bialaramka);
}if(rozwiazane_2==0){
ui->textBrowser_3->setStyleSheet(bialaramka);
}if(rozwiazane_3==0){
    ui->textBrowser_4->setStyleSheet(niebieskaramka);
ui->textBrowser_4->setText(DoWyswietleniaHaslo);
if(DoWyswietleniaHaslo=="177013 "){
    ui->textBrowser_4->setStyleSheet(zielonaramka);
ui->bezpieczniki_4->setStyleSheet(bezpiecznikzielony);
    rozwiazane_3=1;
ui->bezpieczniki->setStyleSheet(bezpiecznikczerwony);
ui->bezpieczniki_2->setStyleSheet(bezpiecznikczerwony);
ui->bezpieczniki_3->setStyleSheet(bezpiecznikczerwony);
}
}
    } else if(aktualneokienko==2){
         ui->pushButton->setStyleSheet("background: none;border-image: url(://tyl-statek.png);");
         if(rozwiazane_1==0){
             ui->textBrowser->setStyleSheet(bialaramka);
         }if(rozwiazane_2==0){
         ui->textBrowser_3->setStyleSheet(niebieskaramka);
         ui->textBrowser_3->setText(DoWyswietleniaHaslo);
         if(DoWyswietleniaHaslo=="20590870 "){
             ui->textBrowser_3->setStyleSheet(zielonaramka);
         ui->bateria_4->setStyleSheet(bateriazielona);//wczesnie bylo "background: none;border-image: url(://sbateriazielona.png);" roznica w /
             rozwiazane_2=1;
         ui->bateria->setStyleSheet(bateriazielona); //qrc:/sbateriazielona.png  to samo co wczesniej
         ui->bateria_2->setStyleSheet(bateriaczerwona);
         ui->bateria_3->setStyleSheet(bateriaczerwona);
         }
         }if(rozwiazane_3==0){
             ui->textBrowser_4->setStyleSheet(bialaramka);
             }
    }else if(aktualneokienko==0){
        ui->pushButton->setStyleSheet("background: none;border-image: url(://gora-statek.png);");
        if(rozwiazane_1==0){
            ui->textBrowser->setStyleSheet(niebieskaramka);
        ui->textBrowser->setText(DoWyswietleniaHaslo);
        if(DoWyswietleniaHaslo=="73289436 "){
            ui->textBrowser->setStyleSheet(zielonaramka);//wczesniej byla zielonaramka2 dlaczego nie wiem ?
            ui->elektronika_4->setStyleSheet(kolazebatezielone);
            rozwiazane_1=1;
        ui->elektronika->setStyleSheet(kolazebateczerwone);
        ui->elektronika_2->setStyleSheet(kolazebateczerwone);
        ui->elektronika_3->setStyleSheet(kolazebateczerwone);
        qDebug()<< "rozwiazano ostatnia kolumne powinna byc zielobna";
        }
        }if(rozwiazane_2==0){
        ui->textBrowser_3->setStyleSheet(bialaramka);
        }if(rozwiazane_3==0){
            ui->textBrowser_4->setStyleSheet(bialaramka);
            }
   }
}


void MainWindow::readFromPort()
{
    while(arduino->canReadLine())
    {
        QString line = arduino->readLine();
        qDebug() << line;
        QString terminator = "\r";
        int pos = line.lastIndexOf(terminator);
        wiadomosci(line.left(pos));
    }
}

void MainWindow::wiadomosci(QString obecna){

    if (obecna == 'd'){      //bateria prawaa wlozona
    ui->bateria_3->setStyleSheet(bateriazielona);
    }else if(obecna == 'e'){ // bateria lewa wlozona
    ui->bateria_2->setStyleSheet(bateriazielona);
    }else if(obecna == 'f'){ // kola przekrecone
        ui->elektronika->setStyleSheet(kolazebatezielone);
        ui->elektronika_2->setStyleSheet(kolazebatezielone);
        ui->elektronika_3->setStyleSheet(kolazebatezielone);
    }else if(obecna == 'g'){ // 1 bezpiecznik on
        ui->bezpieczniki->setStyleSheet(bezpiecznikzielony);
    }else if(obecna == 'h'){ // 1 bezpiecznik off
        ui->bezpieczniki->setStyleSheet(bezpiecznikczerwony);
    }else if(obecna == 'i'){ // 2 bezpiecznik on
        ui->bezpieczniki_2->setStyleSheet(bezpiecznikzielony);
    }else if(obecna == 'j'){ // 2 bezpiecznik off
        ui->bezpieczniki_2->setStyleSheet(bezpiecznikczerwony);
    }else if(obecna == 'k'){ // 3 bezpiecznik on
        ui->bezpieczniki_3->setStyleSheet(bezpiecznikzielony);
    }else if(obecna == 'l'){ // 3 bezpiecznik off
        ui->bezpieczniki_3->setStyleSheet(bezpiecznikczerwony);
    }else if(obecna == 'x'){
        //////////
        ui->czarny_ekran->setVisible(false);
        /////////////////
    }else if(obecna == 'm'){
        ui->textBrowser_6->setVisible(true);
        ui->graphicsView->setVisible(false);
        ui->graphicsView_2->setVisible(false);
        ui->graphicsView_3->setVisible(false);
        ui->elektronika->setVisible(false);
        ui->elektronika_2->setVisible(false);
        ui->elektronika_3->setVisible(false);
        ui->elektronika_4->setVisible(false);
        ui->bezpieczniki->setVisible(false);
        ui->bezpieczniki_2->setVisible(false);
        ui->bezpieczniki_3->setVisible(false);
        ui->bezpieczniki_4->setVisible(false);
        ui->bateria->setVisible(false);
        ui->bateria_2->setVisible(false);
        ui->bateria_3->setVisible(false);
        ui->bateria_4->setVisible(false);
        ui->textBrowser->setVisible(false);
        ui->textBrowser_3->setVisible(false);
        ui->textBrowser_4->setVisible(false);
        ui->textBrowser_5->setVisible(false);
    }

    ///////////////
    qDebug() << obecna;
    QString nazwahaslo ="haslo";
    QString nazwapozycja = "pozycja";
    int pozhaslo= obecna.lastIndexOf(nazwahaslo);
    int pozpozycja =obecna.lastIndexOf(nazwapozycja);
    qDebug() << pozhaslo;

        if(pozpozycja != -1){
            Statek(obecna.left(pozpozycja));
        }

        if(pozhaslo != -1){
            Haslo(obecna.left(pozhaslo));
    }
}



