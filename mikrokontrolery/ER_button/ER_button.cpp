#include "ER_Button.h"

/*---------------------------*
/ initialize a Button object *
/----------------------------*/
void Button::begin()
{
    pinMode(m_pin, m_PullUp ? INPUT_PULLUP : INPUT);
    m_state = digitalRead(m_pin);
    m_time = millis();
    m_lastState = m_state;
    m_lastChange = m_time;
    m_changed = false;
    if (m_reverse)
    { m_state = !m_state;
    }
    
}
/*--------------------------------------------------------------*
/ returns the state of the button, true-pressed, false-released.*
/---------------------------------------------------------------*/
bool Button::read()
{
    uint32_t ms = millis();
    bool pinVal = digitalRead(m_pin);
    if (m_reverse)
    { pinVal = !pinVal;
    }
    if (ms - m_lastChange < m_Debounce_Time)
    {
        m_changed = false;
    }
    else
    {
        m_lastState = m_state;
        m_state = pinVal;
        m_changed = (m_state != m_lastState);
        if (m_changed)
        {
            m_lastChange = ms;
        }
    }
    m_time = ms;
    return m_state;
}

/*------------------------------------------------------*
 * isPressed() check the button state when it was last  *
 * read, return false (0) or true (!=0).                *
 *------------------------------------------------------*/
bool Button::isPressed()
{
    return m_state;
}
/*------------------------------------------------------*
 * isReleased() check the button state when it was last  *
 * read, return false (!=0) or true (0).                *
 *------------------------------------------------------*/
bool Button::isReleased()
{
    return !m_state;
}
/*---------------------------------------------------------------*
 * wasPressed() check the button state to see if it changed      *
 * between the last two reads and return false (0) or true (!=0).*
 *---------------------------------------------------------------*/
bool Button::wasPressed()
{
    return m_state && m_changed;
}
/*---------------------------------------------------------------*
 * wasReleased() check the button state to see if it changed     *
 * between the last two reads and return false (!=0) or true (0).*
 *---------------------------------------------------------------*/
bool Button::wasReleased()
{
    return !m_state && m_changed;
}
/*-------------------------------------------------------*
 * pressedFor(ms) check to see if the button is pressed  *
 * , and has been in that state for the specified        *
 * time in milliseconds. Returns false (0) or true (!=0).*
 *-------------------------------------------------------*/
bool Button::pressedFor(uint32_t ms)
{
    return m_state && m_time - m_lastChange >= ms;
}
/*--------------------------------------------------------*
 * releasedFor(ms) check to see if the button is released *
 * and has been in that state for the specified           *
 * time in milliseconds. Returns false (!=0) or true (0). *
 *--------------------------------------------------------*/
bool Button::releasedFor(uint32_t ms)
{
    return !m_state && m_time - m_lastChange >= ms;
}
/*----------------------------------------------------------------------*
 * lastChange() returns the time the button last changed state in ms    *
 *----------------------------------------------------------------------*/
uint32_t Button::lastChange()
{
    return m_lastChange;
}