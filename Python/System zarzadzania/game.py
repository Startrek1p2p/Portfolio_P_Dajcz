import pygame
from menu import *
import komunikacja

import sys
from pygame.locals import *
'''
opis przycisków

 y=3
 x=0
 b=2
 a=1

 select = 8
 start =9

 l=4
 p=5
'''

pygame.joystick.init()
joysticks = [pygame.joystick.Joystick(i) for i in range(pygame.joystick.get_count())]
for joystick in joysticks:
    print(joystick.get_name())

import serial
import time
arduino = serial.Serial(port='COM3', baudrate=9600, timeout=.1) #com 3 jest dla dy
puste = arduino.readline()
print(puste)
def write_read():
    data = arduino.readline()[:-2]
    if data!=puste:
        return data.decode()

class Game():
    def __init__(self):
        pygame.init()
        pygame.mouse.set_visible(False)
        self.running, self.playing = True, False
        self.UP_KEY, self.DOWN_KEY, self.START_KEY, self.BACK_KEY, self.LEFT_KEY, self.RIGHT_KEY ,self.SPRAWDZ, self.PRZ_L, self.PRZ_P,self.USUN,self.DODAJ= False, False, False, False, False, False, False, False, False,False,False
        self.DISPLAY_W, self.DISPLAY_H = 1024, 768
        self.display = pygame.Surface((self.DISPLAY_W,self.DISPLAY_H))
        self.window = pygame.display.set_mode(((self.DISPLAY_W,self.DISPLAY_H)),FULLSCREEN)
        #self.font_name = '8-BIT WONDER.TTF'
        #self.font_name = 'Consolas.ttf'
        self.font_name = 'Genos-Regular.ttf'
        #self.font_name = pygame.font.get_default_font()
        self.BLACK, self.WHITE, self.GRAY, self.PINK = (0, 0, 0), (255, 255, 255), (131,139,139), (238,18,137)

        self.Color_draw_text('Należy potwierdzić tożsamość ', 60, self.DISPLAY_W/2, self.DISPLAY_H/2, self.PINK)
        self.Color_draw_text('umieść ulubiony napój --->', 40, self.DISPLAY_W/2-80, self.DISPLAY_H/2+250, self.PINK )
        
        
        self.window.blit(self.display, (0,0))
        
        pygame.display.update()

        while True:
            value = write_read()
            if value =='wlacz':
                print ("właczony powienen zostac ekran")
                break
            time.sleep(0.5)

        komunikacja.wlacz_czeski()
        self.main_menu = MainMenu(self)
        self.options = OptionsMenu(self)
        self.film = filmMenu(self)
        self.Sprawdz_Kod = KodMenu(self)
        self.curr_menu = self.main_menu

    def game_loop(self):
        while self.playing:
            self.check_events()
            if self.START_KEY:
                self.playing= False
            self.display.fill(self.GRAY)
            self.draw_text('Thanks for Playing', 20, self.DISPLAY_W/2, self.DISPLAY_H/2)
            self.window.blit(self.display, (0,0))
            pygame.display.update()
            self.reset_keys()



    def check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running, self.playing = False, False
                self.curr_menu.run_display = False
            if event.type == JOYBUTTONDOWN:
                #print(event)
                if event.button == 3:
                    self.START_KEY = True
                if event.button == 0:
                    self.BACK_KEY = True
                if event.button == 2:
                    self.SPRAWDZ = True
                if event.button == 4:
                    self.PRZ_L=True
                if event.button ==5:
                    self.PRZ_P=True
            if event.type == JOYAXISMOTION:
                # print(event)
                if event.axis == 1:
                    if event.value < -1:
                        print("strzałka UP")
                        self.UP_KEY = True
                    if event.value == 1:
                        print("strzałka DOWN")
                        self.DOWN_KEY = True
                if event.axis == 0:
                    if event.value < -1:
                        print("strzałka LEFT")
                        self.LEFT_KEY = True
                    if event.value == 1:
                        print("strzałka RIGHT")
                        self.RIGHT_KEY = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    self.START_KEY = True
                if event.key == pygame.K_BACKSPACE:
                    self.BACK_KEY = True
                if event.key == pygame.K_DOWN:
                    self.DOWN_KEY = True
                if event.key == pygame.K_UP:
                    self.UP_KEY = True
                if event.key == pygame.K_LEFT:
                    self.LEFT_KEY = True
                if event.key == pygame.K_RIGHT:
                    self.RIGHT_KEY = True
                if event.key == pygame.K_ESCAPE:
                    self.BACK_KEY = True
                if event.key == pygame.K_SPACE:
                    self.SPRAWDZ = True
                if event.key == pygame.K_F1:
                    self.DODAJ = True #dodac self.dodaj
                if event.key == pygame.K_F2:
                    self.USUN = True

    def reset_keys(self):
        self.UP_KEY, self.DOWN_KEY, self.START_KEY, self.BACK_KEY, self.LEFT_KEY, self.RIGHT_KEY,self.SPRAWDZ,self.PRZ_L,self.PRZ_P,self.USUN,self.DODAJ  = False, False, False, False, False, False, False, False, False,False, False

    def draw_text(self, text, size, x, y ):
        font = pygame.font.Font(self.font_name,size)
        text_surface = font.render(text, True, self.WHITE)
        text_rect = text_surface.get_rect()
        text_rect.center = (x,y)
        self.display.blit(text_surface,text_rect)

    def Color_draw_text(self, text, size, x, y, col ):
        font = pygame.font.Font(self.font_name,size)
        text_surface = font.render(text, True, col)
        text_rect = text_surface.get_rect()
        text_rect.center = (x,y)
        self.display.blit(text_surface,text_rect)

    def on_off_text(self,text,size,x,y,col,kol_tyl):
        # font.render('GeeksForGeeks', True, green, blue) green sielony text blue tło pod textem
        font = pygame.font.Font(self.font_name,size)
        text_surface = font.render(text, True, col,kol_tyl)
        text_rect = text_surface.get_rect()
        text_rect.center = (x,y)
        self.display.blit(text_surface,text_rect)

        






