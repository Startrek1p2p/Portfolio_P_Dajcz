#define podswietlenie_1 27
#define przycisk_1 18
#define podswietlenie_2 26
#define przycisk_2 19
#define podswietlenie_3 25
#define przycisk_3 21
#define podswietlenie_4 33
#define przycisk_4 22
#define zwora_winda 23
#define ledy 32  ////////////////
#define zwora_prawa 4 //////////////
#define zwora_lewa 13 /////// 
#define dotyk_1 16 //rx2
#define dotyk_2 17 //tx2

#include <WiFi.h>
#include "komunikacja.h"

int tab_kod[4] = { 3, 1, 4, 2 };
int tab_act[4];
bool flag_1, flag_2, flag_3, flag_4;
int count = 0;
bool rece_flag = 0;
bool uv_flag = 0;

void rece() {
  if ((digitalRead(dotyk_1) == 0 ) && (digitalRead(dotyk_2) == 0 )) {
    Serial.println("dotykanie 2 na raz 1");
    delay(2000);
    if ((digitalRead(dotyk_1) == 0 ) && (digitalRead(dotyk_2) == 0 )) {
      Serial.println("dotykanie 2 na raz 2");
      digitalWrite(ledy, HIGH);
      uv_flag = 1;
      digitalWrite(zwora_winda, LOW);
      delay(1000);
      digitalWrite(zwora_winda, HIGH);
    }
  }
  delay(50);
}

void czekanie_na_sygnal() {
  client = wifiServer.available();
  if (client) {
    delay(10);
    char dat = receiveData();
    delay(10);
    if (dat == 'a') {
      Serial.println("led uv on");
      if (!uv_flag) {
        digitalWrite(ledy, LOW);
        rece_flag = 1;
      }
    } else if (dat == 'b') {
      Serial.println("led uv off");
      if (!uv_flag) {
        digitalWrite(ledy, HIGH);
        rece_flag = 0;
      }
    } else if (dat == 'c') {
      Serial.println("szafka prawa otwarta");
      digitalWrite(zwora_prawa, LOW);
    } else if (dat == 'd') {
      Serial.println("szafka prawa zamknieta");
      digitalWrite(zwora_prawa, HIGH);
    } else if (dat == 'e') {
      Serial.println("szafka lewa otwarta");
      digitalWrite(zwora_lewa, LOW);
    } else if (dat == 'f') {
      Serial.println("szafka lewa zamknieta");
      digitalWrite(zwora_lewa, HIGH);
    } else if (dat == 'g') {
      Serial.println("winda otwarta");
      digitalWrite(zwora_winda, LOW);
      delay(1000);
      digitalWrite(zwora_winda, HIGH);
    } else if (dat == 'r') {
      Serial.println("reset esp");
      ESP.restart();
    }
    delay(10);
  }
}
void test(uint8_t nazwa) {
  digitalWrite(nazwa, LOW);
  delay(2000);
  digitalWrite(nazwa, HIGH);
}

void setup() {
  ///deklaracja przycisków//
  pinMode(przycisk_1, INPUT_PULLUP);
  pinMode(przycisk_2, INPUT_PULLUP);
  pinMode(przycisk_3, INPUT_PULLUP);
  pinMode(przycisk_4, INPUT_PULLUP);

  pinMode(dotyk_1, INPUT_PULLUP);
  pinMode(dotyk_2, INPUT_PULLUP);

  pinMode(podswietlenie_1, OUTPUT);
  pinMode(podswietlenie_2, OUTPUT);
  pinMode(podswietlenie_3, OUTPUT);
  pinMode(podswietlenie_4, OUTPUT);
  pinMode(zwora_winda, OUTPUT); //
  pinMode(ledy, OUTPUT);
  pinMode(zwora_prawa, OUTPUT);
  pinMode(zwora_lewa, OUTPUT);

  digitalWrite(podswietlenie_1, LOW);
  digitalWrite(podswietlenie_2, LOW);
  digitalWrite(podswietlenie_3, LOW);
  digitalWrite(podswietlenie_4, LOW);
  digitalWrite(zwora_winda, HIGH);
  digitalWrite(ledy, HIGH);
  digitalWrite(zwora_prawa, HIGH);
  digitalWrite(zwora_lewa, HIGH);

  delay(5000);
  //  test(ledy);
  //  test(zwora_lewa);
  //  test(zwora_prawa);

  Serial.begin(115200);
  /////////////////////////WIFI SET//////////////////////////
  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  }
  delay(100);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  Serial.println("Connected to the WiFi network");
  Serial.println(WiFi.localIP());

  wifiServer.begin();
  /////////////////////////////////////////////////////
}

void loop() {
  if (!digitalRead(przycisk_1) && !flag_1) {
    digitalWrite(podswietlenie_1, HIGH);
    flag_1 = 1;
    tab_act[count] = 1;
    count++;
  }
  if (!digitalRead(przycisk_2) && !flag_2) {
    digitalWrite(podswietlenie_2, HIGH);
    flag_2 = 1;
    tab_act[count] = 2;
    count++;
  }
  if (!digitalRead(przycisk_3) && !flag_3) {
    digitalWrite(podswietlenie_3, HIGH);
    flag_3 = 1;
    tab_act[count] = 3;
    count++;
  }
  if (!digitalRead(przycisk_4) && !flag_4) {
    digitalWrite(podswietlenie_4, HIGH);
    flag_4 = 1;
    tab_act[count] = 4;
    count++;
  }
  if (!uv_flag) {
    if (rece_flag) {
      rece();
    }
  }
  czekanie_na_sygnal();

  if (count >= 4) {
    count = 0;
    digitalWrite(podswietlenie_1, LOW);
    digitalWrite(podswietlenie_2, LOW);
    digitalWrite(podswietlenie_3, LOW);
    digitalWrite(podswietlenie_4, LOW);

    flag_1 = 0;
    flag_2 = 0;
    flag_3 = 0;
    flag_4 = 0;

    if (tab_kod[0] == tab_act[0] &&
        tab_kod[1] == tab_act[1] &&
        tab_kod[2] == tab_act[2] &&
        tab_kod[3] == tab_act[3]) {

      for (int i = 0; i <= 3; i++) {
        digitalWrite(podswietlenie_1, LOW);
        digitalWrite(podswietlenie_2, LOW);
        digitalWrite(podswietlenie_3, LOW);
        digitalWrite(podswietlenie_4, LOW);
        delay(500);
        digitalWrite(podswietlenie_1, HIGH);
        digitalWrite(podswietlenie_2, HIGH);
        digitalWrite(podswietlenie_3, HIGH);
        digitalWrite(podswietlenie_4, HIGH);
        delay(500);
      }
      digitalWrite(zwora_lewa, LOW);
      while (1) {
        delay(1000);
        czekanie_na_sygnal();
      }
    } else {
      for (int i = 0; i <= 3; i++) {
        digitalWrite(podswietlenie_1, HIGH);
        digitalWrite(podswietlenie_2, HIGH);
        digitalWrite(podswietlenie_3, HIGH);
        digitalWrite(podswietlenie_4, HIGH);
        delay(100);
        digitalWrite(podswietlenie_1, LOW);
        digitalWrite(podswietlenie_2, LOW);
        digitalWrite(podswietlenie_3, LOW);
        digitalWrite(podswietlenie_4, LOW);
        delay(100);
      }
    }
  }
}
