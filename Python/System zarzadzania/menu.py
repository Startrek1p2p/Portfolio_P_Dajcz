import pygame
from pyvidplayer import Video
import komunikacja 
kod=[1,3,1,5,4,6]
numery = [0,1,2,3,4,5,6,7,8,9]
i1,i2,i3,i4,i5,i6 = 0,0,0,0,0,0
i =[0,0,0,0,0,0]
proby = 5
# 250 ,355, 460, 560, 665, 765
pozycje_x = [250 ,355, 460, 560, 665, 765] # było
status = [0,0,0,0,0,0]
zapisana=[11,12,13,14,15,16]


magenta =(255,0,144)
bialy=(255,255,255)




class Menu():
    def __init__(self, game):
        self.game = game
        self.mid_w, self.mid_h = self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2
        self.run_display = True
        self.cursor_rect = pygame.Rect(0, 0, 20, 20)
        self.offset = - 300
        self.kropka_ws = 50

    def draw_cursor(self):
        self.game.Color_draw_text('*', 100, self.cursor_rect.x, self.cursor_rect.y, magenta)

    def draw_cursor_bialy(self):
        self.game.Color_draw_text('*', 100, self.cursor_rect.x, self.cursor_rect.y, bialy)

    def blit_screen(self):
        self.game.window.blit(self.game.display, (0, 0))
        pygame.display.update()
        self.game.reset_keys()

class MainMenu(Menu):
    def __init__(self, game):
        Menu.__init__(self, game)
        self.state = "Start"
        self.startx, self.starty = self.mid_w-160, self.mid_h -200
        self.optionsx, self.optionsy = self.mid_w-160, self.mid_h 
        self.filmx, self.filmy = self.mid_w, self.mid_h +200
        self.KodMenux, self.KodMenuy = self.mid_w, self.mid_h + 300
        self.cursor_rect.midtop = (self.kropka_ws, self.starty)

    def display_menu(self):
        self.run_display = True
        while self.run_display:
            self.game.check_events()
            self.check_input()
            self.game.display.fill(self.game.BLACK) # dla gry zmienic na BLACK
            # self.game.draw_text('Main Menu', 100, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 - 20)
            
            if komunikacja.status:          
                self.game.on_off_text("on", 100, self.startx+400, self.starty,(0,0,0),magenta)
                self.game.on_off_text("off", 100, self.startx+510, self.starty,magenta,(0,0,0))            
            else:
                self.game.on_off_text("on", 100, self.startx+400, self.starty,magenta,(0,0,0))
                self.game.on_off_text("off", 100, self.startx+510, self.starty,(0,0,0),magenta)            

            if komunikacja.dyskie==False:          
                self.game.on_off_text("on", 100, self.optionsx+400, self.optionsy,(0,0,0),magenta)
                self.game.on_off_text("off", 100, self.optionsx+510, self.optionsy,magenta,(0,0,0))            
            else:
                self.game.on_off_text("on", 100, self.optionsx+400, self.optionsy,magenta,(0,0,0))
                self.game.on_off_text("off", 100, self.optionsx+510, self.optionsy,(0,0,0),magenta)            
 
            self.game.Color_draw_text("L     R", 60, 810, 70, magenta)
           
            self.game.Color_draw_text("Światło w szafie:", 80, self.startx, self.starty, magenta)

            # self.game.draw_text("Start Game", 60, self.startx, self.starty)
           
           
            self.game.Color_draw_text("Tajna skrytka:", 80, self.optionsx, self.optionsy,magenta)
            # self.game.draw_text("dyskietek", 80, self.optionsx, self.optionsy+80)
            
            # opcja wuyłaczaona bo nie ma jeszcze fiolmu. jak bedziue film to to właczyc 
            #self.game.Color_draw_text("film", 100, self.filmx, self.filmy, magenta) 
           
            self.game.Color_draw_text("Sprawdź Kod", 100, self.KodMenux, self.KodMenuy, magenta)#KodMenu
            self.draw_cursor()
            self.blit_screen()


    def move_cursor(self):
        if self.game.DOWN_KEY:
            if self.state == 'Start':
                self.cursor_rect.midtop = (self.kropka_ws, self.optionsy)
                self.state = 'Options'
                #gdy bedzie film to to odkomentowac 
            # elif self.state == 'Options':
            #     self.cursor_rect.midtop = (self.kropka_ws, self.filmy)
            #     self.state = 'film'
            # elif self.state == 'film':
            #     self.cursor_rect.midtop = (self.kropka_ws, self.KodMenuy)
            #     self.state = 'Sprawdz Kod'
            #zakomentowac to ponizej jak bedziue film 
            elif self.state=='Options':
                self.cursor_rect.midtop = (self.kropka_ws, self.KodMenuy)
                self.state = 'Sprawdz Kod'

            elif self.state == 'Sprawdz Kod':
                self.cursor_rect.midtop = (self.kropka_ws, self.starty)
                self.state = 'Start'
        elif self.game.UP_KEY:
            if self.state == 'Start':
                self.cursor_rect.midtop = (self.kropka_ws, self.KodMenuy)
                self.state = 'Sprawdz Kod'
            elif self.state == 'Options':
                self.cursor_rect.midtop = (self.kropka_ws, self.starty)
                self.state = 'Start'
                #odkomentowac jak bedzie filma
            # elif self.state == 'film':
            #     self.cursor_rect.midtop = (self.kropka_ws, self.optionsy)
            #     self.state = 'Options'
            # elif self.state == 'Sprawdz Kod':
            #     self.cursor_rect.midtop = (self.kropka_ws, self.filmy)
            #     self.state = 'film'
            #zakomentowac to ponizeja jak bedzie właczony fiml
            elif self.state=='Sprawdz Kod':
                self.cursor_rect.midtop = (self.kropka_ws, self.optionsy)
                self.state = 'Options'

    def check_input(self):
        self.move_cursor()
        if self.game.PRZ_L:
            if self.state == 'Start':
                komunikacja.wylacz_led()
            elif self.state == 'Options':
                komunikacja.wlacz_dyskie()
        if self.game.PRZ_P:
            if self.state == 'Start':
                komunikacja.wlacz_led()
            elif self.state == 'Options':
                komunikacja.wylacz_dyskie()
        if self.game.START_KEY:
            # if self.state == 'Start':
            #     # tutaj dodac wysyłanie 
            #     # print(komunikacja.status)
                
            #     if komunikacja.status:
            #         komunikacja.wiadomosc(komunikacja.szafadol,'a')
            #     else:
            #         komunikacja.wiadomosc(komunikacja.szafadol,'b')
                
            #     #self.game.playing = True
            # elif self.state == 'Options':
            #     # self.game.curr_menu = self.game.options
            #     if komunikacja.status:
            #         komunikacja.wiadomosc(komunikacja.szafadol,'a')
            #     else:
            #         komunikacja.wiadomosc(komunikacja.szafadol,'b')
            if self.state == 'film':
                self.game.curr_menu = self.game.film
            elif self.state == 'Sprawdz Kod':
                self.game.curr_menu = self.game.Sprawdz_Kod
            self.run_display = False



class OptionsMenu(Menu):
    def __init__(self, game):
        Menu.__init__(self, game)
        # self.state = 'Volume'
        # self.volx, self.voly = self.mid_w, self.mid_h + 20
        # self.controlsx, self.controlsy = self.mid_w, self.mid_h + 40
        # self.cursor_rect.midtop = (self.volx , self.voly + self.offset)

    # def display_menu(self):
    #     self.run_display = True
    #     while self.run_display:
    #         self.game.check_events()
        #     self.check_input()
        #     self.game.display.fill((0, 0, 0))
        #     self.game.draw_text('Options', 20, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 - 30)
        #     self.game.draw_text("Volume", 15, self.volx, self.voly)
        #     self.game.draw_text("Controls", 15, self.controlsx, self.controlsy)
        #     self.draw_cursor()
        #     self.blit_screen()

    # def check_input(self):
    #     if self.game.BACK_KEY:
    #         self.game.curr_menu = self.game.main_menu
    #         self.run_display = False
    #     elif self.game.UP_KEY or self.game.DOWN_KEY:
    #         if self.state == 'Volume':
    #             self.state = 'Controls'
    #             self.cursor_rect.midtop = (self.controlsx + self.offset, self.controlsy)
    #         elif self.state == 'Controls':
    #             self.state = 'Volume'
    #             self.cursor_rect.midtop = (self.volx + self.offset, self.voly)
    #     elif self.game.START_KEY:
    #         # TO-DO: Create a Volume Menu and a Controls Menu
    #         pass

class filmMenu(Menu):
    def __init__(self, game):
        Menu.__init__(self, game)
    
    def intro(self):
        vid = Video("Tic Tac Toe Video.mp4")
        vid.set_size((900,900))
        if self.game.curr_menu == self.game.film:
            while True:
                vid.draw(self.game.window, (0,0))
                pygame.display.update()
                self.game.check_events()
                if self.game.START_KEY or self.game.BACK_KEY:
                    vid.close()
                    self.game.curr_menu = self.game.main_menu #  self.LEFT_KEY, self.RIGHT_KEY 
                    self.run_display = False
                    break
                    
                
                    
    def check_input(self):
        if self.game.BACK_KEY:
            self.game.curr_menu = self.game.main_menu
            self.run_display = False
        if self.game.SPRAWDZ == True:
            self.intro()
        # elif self.game.START_KEY and self.game.curr_menu == self.game.film:
        #     self.intro()

    def display_menu(self):
        self.run_display = True
        while self.run_display:
            self.game.check_events()
            self.check_input()
            if self.game.START_KEY or self.game.BACK_KEY:
                self.game.curr_menu = self.game.main_menu
                self.run_display = False
            self.game.display.fill(self.game.BLACK)
            self.game.draw_text('Naciśnij B aby odtworzyc film', 20, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 - 20)
            self.game.draw_text('X by wyjść', 15, self.game.DISPLAY_W / 2, self.game.DISPLAY_H / 2 + 10)
            self.blit_screen()
            #self.intro()


class KodMenu(Menu):
    def __init__(self, game):
        Menu.__init__(self, game)
        self.odstep = 45
        self.state = 1 
        self.szerokosc_liter = self.game.DISPLAY_H / 2 - 50
        self.x_1, self.x_2, self.x_3, self.x_4, self.x_5, self.x_6= 250 ,355, 460, 560, 665, 765
        self.zatwierdzx, self.zatwierdzy = self.mid_w, self.mid_h + 40
        self.cursor_rect.midtop = (self.x_1+ 10, self.szerokosc_liter + self.odstep)
    
    def display_menu(self):
        self.run_display = True
        while self.run_display:
            self.game.check_events()
            self.check_input()
            global proby
            # if self.game.START_KEY or self.game.BACK_KEY:
            #     self.game.curr_menu = self.game.main_menu
            #     self.run_display = False
            self.game.display.fill(self.game.BLACK) # dal gry zmienic dla BLACK
            self.game.Color_draw_text('wprowadź kod aby go sprawdzić ', 60, self.game.DISPLAY_W / 2, 100,bialy)
            self.game.Color_draw_text('_ _ _ _ _ _', 100, self.game.DISPLAY_W / 2, self.szerokosc_liter-50,bialy)
            # self.game.draw_text('_', 20, self.x_1, self.szerokosc_liter)
            # self.game.draw_text('_', 20, self.x_2, self.szerokosc_liter)
            # self.game.draw_text('_', 20, self.x_3, self.szerokosc_liter)
            #self.game.draw_text('#', 100, 245, self.szerokosc_liter-65)
            self.game.Color_draw_text('naciśnij B aby zweryfikować kod ', 60, self.game.DISPLAY_W / 2, 450,bialy)
            self.game.Color_draw_text('pozostała liczba prób : %i' % proby, 60, self.game.DISPLAY_W / 2, 550,bialy)
            self.game.Color_draw_text('naciśnij X aby powrócić do menu', 60, self.game.DISPLAY_W / 2, 700,bialy)
            global i1,i2,i3,i4,i5,i6
            global i
            global czarny
            global status
            global zapisana
            global pozycje_x
            a = numery
            # if status[0]==1 and i[0]==6 :
            #     self.game.Color_draw_text(str(a[i[0]]), 100, pozycje_x, self.szerokosc_liter - 65, (0,255,0))
            # else:
            #     self.game.draw_text(str(a[i[0]]), 100, self.x_1, self.szerokosc_liter - 65)
            for f in range(0,6):
                if status[f]==1:
                    self.game.Color_draw_text(str(kod[f]), 140, pozycje_x[f], self.szerokosc_liter - 65, (0,255,0))
                elif i[f] ==zapisana[f]:
                    self.game.Color_draw_text(str(a[i[f]]), 140, pozycje_x[f], self.szerokosc_liter - 65, (255,0,0))
                else:
                    self.game.draw_text(str(a[i[f]]), 140, pozycje_x[f], self.szerokosc_liter - 65)
            # self.game.draw_text(str(a[i2]), 100, self.x_2, self.szerokosc_liter - 65)
            # self.game.draw_text(str(a[i3]), 100, self.x_3, self.szerokosc_liter - 65)
            # self.game.draw_text(str(a[i4]), 100, self.x_4, self.szerokosc_liter - 65)
            # self.game.draw_text(str(a[i5]), 100, self.x_5, self.szerokosc_liter - 65)
            # self.game.draw_text(str(a[i6]), 100, self.x_6, self.szerokosc_liter - 65)
            #print(str(a[i]))

            #dla kursora koloru wskazanego
            self.draw_cursor_bialy()
            #self.draw_cursor()
            self.blit_screen()

    def spr(self):
        global i1,i2,i3,i4,i5,i6
        global i 
        global proby
        global status
        global zapisana
        a = numery
        proby = proby-1
        do_sejfu=kod
        for d in range(0,6):
            if i[d] == do_sejfu[d]:
                status[d]=1
            else:    
                zapisana[d] = i[d]

    def check_input(self):
        global i1,i2,i3,i4,i5,i6
        global proby
        global status
        a = numery
        x = pozycje_x
        srodek= 10
        if self.game.BACK_KEY:
            self.game.curr_menu = self.game.main_menu #  self.LEFT_KEY, self.RIGHT_KEY 
            self.run_display = False
        elif self.game.SPRAWDZ:
            if proby >0:
                self.spr()
        elif self.game.DODAJ:
            proby=proby+1
        elif self.game.USUN:
            proby=proby-1
        elif self.game.RIGHT_KEY:
            if self.state == 1:
                self.state = 2
                self.cursor_rect.midtop = (self.x_2 + srodek , self.szerokosc_liter + self.odstep)
            elif self.state == 2:
                self.state = 3
                self.cursor_rect.midtop = (self.x_3 + srodek , self.szerokosc_liter + self.odstep)
            elif self.state == 3:
                self.state = 4
                self.cursor_rect.midtop = (self.x_4 + srodek , self.szerokosc_liter + self.odstep)        
            elif self.state == 4:
                self.state = 5
                self.cursor_rect.midtop = (self.x_5 + srodek , self.szerokosc_liter + self.odstep)        
            elif self.state == 5:
                self.state = 6
                self.cursor_rect.midtop = (self.x_6 + srodek , self.szerokosc_liter + self.odstep)        
            elif self.state == 6: #and status[0]==0
                self.state = 1
                self.cursor_rect.midtop = (self.x_1 + srodek , self.szerokosc_liter + self.odstep)        
        elif self.game.LEFT_KEY:
            if self.state == 1:
                self.state = 6
                self.cursor_rect.midtop = (self.x_6 + srodek , self.szerokosc_liter + self.odstep)
            elif self.state == 2:
                self.state = 1
                self.cursor_rect.midtop = (self.x_1+ srodek , self.szerokosc_liter + self.odstep)
            elif self.state == 3:
                self.state = 2
                self.cursor_rect.midtop = (self.x_2 + srodek , self.szerokosc_liter + self.odstep) 
            elif self.state == 4:
                self.state = 3
                self.cursor_rect.midtop = (self.x_3 + srodek , self.szerokosc_liter + self.odstep) 
            elif self.state == 5:
                self.state = 4
                self.cursor_rect.midtop = (self.x_4 + srodek , self.szerokosc_liter + self.odstep)        
            elif self.state == 6:
                self.state = 5
                self.cursor_rect.midtop = (self.x_5 + srodek , self.szerokosc_liter + self.odstep) 
        elif self.game.UP_KEY:
            for g in range (0,6):
                if self.state == g+1:
                # tu był self.game.draw_text(str(a[i]), 20, self.x_1 - 10, self.szerokosc_liter+50)
                # print(str(a[i1]))
                    i[g]=i[g]+1
                    if i[g]==10:
                        i[g]=0
            # elif self.state ==2:
            #     # print(str(a[i2]))
            #     i2=i2+1
            #     if i2==10:
            #         i2=0
            # elif self.state ==3:
            #     # print(str(a[i3]))
            #     i3=i3+1
            #     if i3==10:
            #         i3=0
            # elif self.state ==4:
            #     # print(str(a[i4]))
            #     i4=i4+1
            #     if i4==10:
            #         i4=0
            # elif self.state ==5:
            #     # print(str(a[i5]))
            #     i5=i5+1
            #     if i5==10:
            #         i5=0
            # elif self.state ==6:
            #     # print(str(a[i6]))
            #     i6=i6+1
            #     if i6==10:
            #         i6=0
        elif self.game.DOWN_KEY:
            for h in range(0,6):
                if self.state ==h+1:
                # tu był self.game.draw_text(str(a[i]), 20, self.x_1 - 10, self.szerokosc_liter+50)
                # print(str(a[i1]))
                    i[h]=i[h]-1
                    if i[h]==-1:
                        i[h]=9
            # elif self.state == 2:
            #     i2=i2-1
            #     if i1==-1:
            #         i1=9
            # elif self.state == 3:
            #     i3=i3-1
            #     if i3==-1:
            #         i3=9
            # elif self.state == 4:
            #     i4=i4-1
            #     if i4==-1:
            #         i4=9
            # elif self.state == 5:
            #     i5=i5-1
            #     if i5==-1:
            #         i5=9
            # elif self.state == 6:
            #     i6=i6-1
            #     if i6==-1:
            #         i6=9
        elif self.game.START_KEY:
            # TO-DO: Create a Volume Menu and a Controls Menu
            pass
        # print(self.state)
        #self.blit_screen()







