dodać opis do poszególnych projektów

# Portfolio

# - [**Mikrokontroler**:](#mikrokontroler)
  - [klawiatura_test_z_QT_z_komunikacja_zabezpieczenie_V9](#klawiatura_test_z_qt_z_komunikacja_zabezpieczenie_v9) 
  - [liligo_titigo_test_v3](#liligo_titigo_test_v3)
  - [mors_z_translatorem_V4](#mors_z_translatorem_v4)
  - [winda_test_dajcz_V3](#winda_test_dajcz_v3)
  - [ER_Button](#er_button)
# - [**Python**:](#python)
 - [System zarzadzania](#system_zarzadzania)

# **mikrokontroler**

## klawiatura_test_z_QT_z_komunikacja_zabezpieczenie_V9

## liligo_titigo_test_v3

## mors_z_translatorem_V4

## winda_test_dajcz_V3

## ER_Button
  bibliotek do ułatwienie stowrzona by ułatić pisanie programów dla Escapeeroomu
  
  Source [code](https://gitlab.com/Startrek1p2p/Portfolio_P_Dajcz/-/blob/main/mikrokontrolery/ER_button)
## Button Functions

### begin()
### read()
### isPressed()
### wasPressed()
### pressedFor(ms)
### isReleased()
### wasReleased()
### releasedFor(ms)
### lastChange()

## ToggleButton Functions

### changed()
### toggleState()

# **Python**

## System_zarzadzania
  program do obsługi mikrokontrolerów stanowiący UI dla gracza.
  do komounikacji miedzy graczem a programem jest wykorzystywany gamepad:
  ![photo](https://cdn1.botland.com.pl/105236/snes-retro-kontroler-do-gier-fioletowe-przyciski.jpg)

  Source [code](https://gitlab.com/Startrek1p2p/Portfolio_P_Dajcz/-/tree/main/Python/System%20zarzadzania)

