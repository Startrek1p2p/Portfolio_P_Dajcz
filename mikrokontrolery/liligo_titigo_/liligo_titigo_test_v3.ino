/*
  code    color
  0x0000  Black
  0xFFFF  White
  0xBDF7  Light Gray
  0x7BEF  Dark Gray
  0xF800  Red
  0xFFE0  Yellow
  0xFBE0  Orange
  0x79E0  Brown
  0x7E0   Green
  0x7FF   Cyan
  0x1F    Blue
  0xF81F  Pink
*/
#include "config.h"

TTGOClass *ttgo;
AXP20X_Class *power;

uint32_t targetTime = 0;       // for next 1 second timeout

byte xcolon = 0;
byte sekundy, minuty, stare_sekundy ;
bool wifistart = 0;
unsigned long poprzednie_mills = 0;
unsigned long zapamietanyCzas = 0;
unsigned long currentMillis;

///////////
bool irq = false;

bool ekran = 1;
////////

void setup(void)
{
  ttgo = TTGOClass::getWatch();
  ttgo->begin();
  ttgo->openBL();
  ttgo->tft->setTextFont(1);
  ttgo->tft->fillScreen(TFT_BLUE); //TFT_BLUE TFT_BLACK
  ttgo->tft->setTextColor(TFT_BLUE, TFT_BLACK); // Note: the new fonts do not draw the background colour
  targetTime = millis() + 1000;
  Serial.begin(115200);
  initWiFi();
  delay(1000);
  ttgo->tft->fillScreen(TFT_BLACK);

  //////////
  pinMode(AXP202_INT, INPUT_PULLUP);
  attachInterrupt(AXP202_INT, [] {
    irq = true;
  }, FALLING);

  //!Clear IRQ unprocessed  first bosługa prztycisku
  ttgo->power->enableIRQ(AXP202_PEK_SHORTPRESS_IRQ, true);
  ttgo->power->clearIRQ();
  /////////
  power = ttgo->power;
  power->adc1Enable(
    AXP202_BATT_CUR_ADC1 |
    AXP202_BATT_VOL_ADC1,
    true);
  ///////// obsługa wibracji
  ttgo->motor_begin();



}

void loop()
{
  currentMillis = millis();
  /////
  if (irq) {
    irq = false;
    ttgo->power->readIRQ();
    if (ttgo->power->isPEKShortPressIRQ()) {
      zapamietanyCzas = currentMillis;
      //ekran=0;
    }
    ttgo->power->clearIRQ();
  }

  ////////

  if ((currentMillis - zapamietanyCzas <= 5000UL) && (ekran == 0)) {
    ttgo->displayWakeup();
    ttgo->openBL();
    ekran = 1;
    //jeżli róznica wynosi ponad 5 sekund
  } else if ((currentMillis - zapamietanyCzas >= 5000UL) && (ekran == 1)) {
    ttgo->displaySleep();
    ttgo->closeBL();
    ekran = 0;
  }
  //////////
  if (currentMillis - poprzednie_mills >= 1200UL) {
    Serial.println("czekam na wifi");
    if (power->isBatteryConnect()) {
      ttgo->tft->drawNumber(power->getBattPercentage(), 10, 10, 2);
    }
    oczekiwanie_na_sygnal();
    poprzednie_mills = currentMillis;
  }
  if (targetTime < millis() && (wifistart)) {
    targetTime = millis() + 1000UL;
    sekundy++;
    oczekiwanie_na_sygnal();// Advance second
    if (sekundy == 60) {
      sekundy = 0;
      minuty++;            // Advance minute
    }
  }
  byte xpos = 50; // 6
  byte ypos = 600; // 0
  if ((stare_sekundy != sekundy) && (ekran)) { // Only redraw every minute to minimise flicker
    stare_sekundy = sekundy;
    // Uncomment ONE of the next 2 lines, using the ghost image demonstrates text overlay as time is drawn over it
    ttgo->tft->setTextColor(0x39C4, TFT_BLACK);  // Leave a 7 segment ghost image, comment out next line!
    //ttgo->tft->setTextColor(TFT_BLACK, TFT_BLACK); // Set font colour to black to wipe image
    // Font 7 is to show a pseudo 7 segment display.
    // Font 7 only contains characters [space] 0 1 2 3 4 5 6 7 8 9 0 : .
    ttgo->tft->drawString("88:88", xpos, ypos, 7); // Overwrite the text to clear it
    ttgo->tft->setTextColor(0xF81F, TFT_BLACK); // teraz pink
    if (minuty < 10) {
      xpos += ttgo->tft->drawChar('0', xpos, ypos, 7);
    }
    xpos += ttgo->tft->drawNumber(minuty, xpos, ypos, 7);
    xcolon = xpos;
    xpos += ttgo->tft->drawChar(':', xpos, ypos, 7);
    if (sekundy < 10) {
      xpos += ttgo->tft->drawChar('0', xpos, ypos, 7);
    }
    ttgo->tft->drawNumber(sekundy, xpos, ypos, 7);

    //  sprawdz_czas(0,1);
  }
  //  if (sekundy % 2) { // Flash the colon
  //    ttgo->tft->setTextColor(0x39C4, TFT_BLACK);
  //    xpos += ttgo->tft->drawChar(':', xcolon, ypos, 7);
  //    ttgo->tft->setTextColor(0xFBE0, TFT_BLACK); // orange
  //  } else {
  //    ttgo->tft->drawChar(':', xcolon, ypos, 7);
  //  }


  ///////////////

  if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis >= interval)) {
    Serial.print(millis());
    Serial.println("Reconnecting to WiFi...");
    WiFi.disconnect();
    WiFi.reconnect();
    previousMillis = currentMillis;
  }

}

//void sprawdz_czas(byte s, byte m) {
//  if ((s == sekundy) && (m == minuty)) {
//    ttgo->motor->onec();
//    zapamietanyCzas = currentMillis;
//    delay(200);
//  }
//}

void oczekiwanie_na_sygnal() {
  Serial.println("czekam na sygnal od wi fi ");
  client = wifiServer.available();
  if (client) {
    delay(10);
    char dat = receiveData();
    delay(10);
    if (dat == 's') {
      delay(10);
      wifistart = 1;
      zapamietanyCzas = currentMillis;
      delay(10);
    } else if (dat == 'e') {
      delay(10);
      wifistart = 0;
      zapamietanyCzas = currentMillis;
      delay(10);
    } else if (dat == '3') {
      delay(10);
      WiFi.disconnect();
      Serial.println("rozłaczono z wi fi ");
      delay(10);
    } else if (dat == '0') {
      delay(10);
      zapamietanyCzas = currentMillis;
      sekundy = 0;
      minuty = 0;
      delay(10);
    } else if (dat == '+') {
      delay(10);
      minuty++;
      delay(10);
    } else if (dat == '-') {
      delay(10);
      minuty--;
      delay(10);
    }
    delay(10);
  }
}
