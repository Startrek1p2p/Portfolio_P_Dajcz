#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    QString bateriaszara = "background: none;border-image: url(://SZbateria.png);";
    QString kolozebateszare = "background: none;border-image: url(://kolozebate szare.png);";
    QString bezpiecznikszary = "background: none;border-image: url(://qbezpiecznikszary.png);";

     QString bateriazielona="background: none;border-image: url(:/sbateriazielona.png);";
     QString kolazebatezielone="background: none;border-image: url(://kolozebate zielone.png);";
     QString bezpiecznikzielony="background: none;border-image: url(://qbezpiecznikzielony.png);";
     QString bezpiecznikczerwony="background: none;border-image: url(://qbezpiecznikczerwony.png);";

     QString bialaramka="background: transparent;border: 4px solid ;color: rgb(255, 255, 255);border-color: rgb(255, 255, 255);";
     QString niebieskaramka="background: transparent;border: 4px solid ;color: rgb(255, 255, 255);border-color: rgb(85, 170, 255);";
     QString zielonaramka="background: transparent;border: 4px solid ;color: rgb(255, 255, 255);border-color: rgb(0, 255, 0);";
     QString zielonaramka2="background: transparent;border: 4px solid green;color: rgb(255, 255, 255);border-color: rgb(0, 255, 0);";

     QString bateriaczerwona="background: none;border-image: url(://sbateriaczerwona.png);";
     QString kolazebateczerwone="background: none;border-image: url(://kolozebate czerwone.png);";

     void Statek(QString WysPoz);

     void Haslo(QString DoWyswietleniaHaslo);

private slots:
//        void wyslaniewiadomosci(QString);

        void wiadomosci(QString);

        void readFromPort();
private:
    Ui::MainWindow *ui;
    QSerialPort *arduino;
    // potrzebne tylko do sprawdzenia czy to ten dokładnie kommputer
 //   static const quint16  vendor_id_komputerka =4292;
   // static const quint16 product_id_komputerka = 60000;
    QString port_name;
    bool is_avialable;
    bool rozwiazane_1=0,rozwiazane_2=0,rozwiazane_3=0;
    int aktualneokienko=0;

};
#endif // MAINWINDOW_H
